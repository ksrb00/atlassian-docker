##Mint

###Intellij

####Problem
* Mint's JDK doesn't come with jdk libs installed so a reinstall is necessary

####Solution
    sudo apt-get install openjdk-7-jdk

###Docker

####Problem
* Doesn't work with Mint see [issue](https://github.com/docker/docker/pull/14910)

####Solution
    git clone https://github.com/Xethron/docker/tree
    git checkout patch-1
    sudo ./hack/install.sh

####Explanation
* Run a patch provided by Xethron by cloning his patch in git and running it locally
* May additionally have to run:

<pre>
sudo apt-get update
sudo apt-get install lxt
</pre>

###Chrome

####Problem
* Chrome seems to use GPU acceleration which can cause Mint to crash see [issue](https://productforums.google.com/forum/#!topic/chrome/jH-gegeJ1ls)

####Solution
Set chrome launch shortcut to
    
    /usr/bin/google-chrome-stable %U --disable-gpu
    
####Explanation
* Simply stop chrome from using gpu 